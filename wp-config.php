<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'projekt1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aCvhETsjKIMa~HD!d{)vC-kB}G1%cbFWi?U%;,26J;lY2C2L7!wJ4VAqSg?~R?mm' );
define( 'SECURE_AUTH_KEY',  '(<l$c9UsV[esk,CJn]_O,+SY9r6]9L@CxlV4%seFD83mF,NfbnK[]zsZv:[!lcS+' );
define( 'LOGGED_IN_KEY',    'b6CTGV^8BIwl9j.#4y.?+|JEH;RX<35%He{y*4`ds*wEI0cM,oN]oh=WW|Tpk$6d' );
define( 'NONCE_KEY',        '!s%B5Z$c/Y4<LOVVtbz,h7{2&]T~8l=9vI CaUpO%_.@z$g=M{b^YSfZVsQMQsT@' );
define( 'AUTH_SALT',        '[-k8x]= V_ur-wc.J:$@aLP%0[;.l.fy;VsLi|A:C~/:BaP+GPKZEkD;/|UfRs7{' );
define( 'SECURE_AUTH_SALT', ':!/d1fp]_ieY%JQ*$}?hOvFL_22<j:=TK9y#yY]eyq:U!b[D~Gl+kK/U?;Ym$|],' );
define( 'LOGGED_IN_SALT',   '/O9XAD/>->m0&os5)J|#FX]B.oV5LAo${{?ib?7*v{Ro@bx(,^Z);0JJl~;7&yAQ' );
define( 'NONCE_SALT',       '6RYl~#J0n(iWdT5_4P5b?4]&*Ekq~f5M`uN,_;zIcV_:-eNjgHi/v4T-}y[2{iy`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
