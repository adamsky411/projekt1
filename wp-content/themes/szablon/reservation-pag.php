<?php
add_reservation();
get_header();
/**
 * Template name: rezerwacja
 */
?>
<div>
<form method="POST">
    <p>
        <select name="re_user" id="re_user">
        <?php echo output_user_options(); ?>
        </select>
    </p>
    <p>
        <select name="re_products[]" id="re_products" multiple>
        <?php echo output_product_options(); ?>
        </select>
    </p>

    <p> <label for="re_post_title"><?php _e('titl:', 'mytextdomain') ?></label>
        <input type="text" name="re_post_title" id="re_post_title" />
    </p>
    <p> <label for="re_dataOd"><?php _e('od kiedy:', 'mytextdomain') ?></label>
        <input id="re_dataOd" name="re_dataOd" type="date"/>
    </p>
    <p>
        <label for="re_dataDo"><?php _e('do kiedy:', 'mytextdomain') ?></label>
        <input id="re_dataDo" type="date" name="re_dataDo"" />
    </p>
    <p>
        <label for="re_price"><?php _e('cena:', 'mytextdomain') ?></label>
        <input id="re_price" type="number" name="re_price"/>
    </p>
    <p>Wybierz pliks: <input name="uploaded" type="file" /><br /></p>
    <p>
    <div class="g-recaptcha" data-sitekey="6LeQaKMUAAAAAFftIwW7ADHIywdi0IbztXmHd55M"></div>
    </p>

    <button type="submit"><?php _e('Submit', 'mytextdomain') ?></button>
    <input type="hidden" name="re_post_type" id="re_post_type" value="reservation" />
    <input type="hidden" name="re_content" id="re_content" value="content"/>


    <?php wp_nonce_field( 'name_of_nonce_field', 'name_of_my_action' ); ?>


</form>
<?php
get_footer();
?>


