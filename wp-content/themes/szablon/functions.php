<?php
define('TEMP_URI', esc_url( get_template_directory_uri() ));
define('TEMP_VAR',get_template_directory());

require_once("functions/thumbnails.inc.php");
require_once("functions/wp_queries.inc.php");
require_once("functions/metabox.php");
require_once("functions/admin-menu.php");
require_once("functions/functions.php");
require_once("functions/postTypes/loader.php");
//require_once("functions/postTypes/users.php");
require_once("functions/add-reservation.php");

