<?php
if(! defined( 'ABSPATH' )) return;
function generateRandomString($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGIJKLMNOPRSTUWYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }

    return $randomString;
}
function set_html_content_type()
{
    return 'text/html';
}

function get_id_by_template_filename($filename)
{
	global $wpdb;
	return $wpdb->get_var("SELECT wp_pm.post_id FROM {$wpdb->posts} wp_p JOIN {$wpdb->postmeta} wp_pm ON wp_p.ID=wp_pm.post_id WHERE wp_p.post_type='page' AND wp_p.post_status='publish' AND meta_key='_wp_page_template' AND meta_value='$filename' ");
}

function print_r_e($var)
{
    echo "<pre>";
	echo htmlspecialchars(print_r($var,true));
    echo "</pre>";
}
include(WP_CONTENT_DIR . "/custom-shortcode.php");
