<?php

class Reservation extends SF_abstract_post{
	public static $name = 'Rezerwacje';
	public static $slug = 'reservation';

	public function createPost() {
		$labels = array(
			'name' => _x(self::$name, 'gabinety'),
            'singular_name' => _x(self::$name, 'gabinety'),
			'menu_name' => _x(self::$name, 'gabinety'),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
            'supports' => array('title', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 26,
			'menu_icon' => 'dashicons-welcome-add-page',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => false,
			'query_var' => true,
			'can_export' => true,
			'capability_type' => 'post'
		);

		register_post_type(self::$slug, $args);
	}

	public function createMeta($meta_boxes){
		$prefix = 're_';

		$meta_boxes[] = array(
			'id'         => 'standard2',
			'title'      => __( 'Podstawowe dane' ),
			'post_types' => array( self::$slug ),
			'context'    => 'normal',
			'priority'   => 'low',
			'autosave'   => true,
			'fields'     => array(

                array(
                    'name'            => 'Select',
                    'id'              => "{$prefix}select",
                    'type'            => 'post',
                    'post_type'       => 'users',

                ),


                array(
                    'name'       => 'Od',
                    'id'         => "{$prefix}dataOd",
                    'type'       => 'date',


                    'js_options' => array(
                        'dateFormat'      => 'yy-mm-dd',
                        'showButtonPanel' => false,
                    ),

                    // Display inline?
                    'inline' => false,

                    // Save value as timestamp?
                    'timestamp' => false,
                ),
                array(
                    'name'       => 'Do',
                    'id'         => "{$prefix}dataDo",
                    'type'       => 'date',


                    'js_options' => array(
                        'dateFormat'      => 'yy-mm-dd',
                        'showButtonPanel' => false,
                    ),

                    // Display inline?
                    'inline' => false,

                    // Save value as timestamp?
                    'timestamp' => false,
                ),
                array(
                    'name' => 'Kwota',
                    'id'   => "{$prefix}price",
                    'type' => 'number',

                    'min'  => 0,
                    'step' => 10,
                ),
                array(
                    'name'            => 'produkty',
                    'id'              => "{$prefix}select_products",
                    'type'            => 'post',
                    'post_type'       => 'products',
                    //'clone'           => true,
                    'multiple'        => true,

                ),
                array(
                    'id'               => "{$prefix}file",
                    'name'             => 'Jakiś plik',
                    'type'             => 'file_advanced',
                    'force_delete'     => false,

                    'max_file_uploads' => 1,

                    'max_status'       => 'false',
                ),
			)
		);
		return $meta_boxes;
	}


}