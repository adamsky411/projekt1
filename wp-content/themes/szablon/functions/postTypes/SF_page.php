<?php
class SF_page extends SF_abstract_post {
	public static $name = 'page';
	public static $slug = 'page';
	public function createPost() {
	}


	public function createMeta($meta_boxes){
		$prefix = 'sf_';

			$meta_boxes[] = array(
				'id'         => 'standard2',
				'title'      => __( 'Podstawowe dane' ),
				'post_types' => array( self::$slug ),
				'context'    => 'normal',
				'priority'   => 'low',
				'autosave'   => true,
				'fields'     => array(

					array(
						'name' => __( 'Zdjęcie w nagłówku', 'gabinety' ),
						'id'   => "{$prefix}image",
						'type' => 'image_advanced',
						'max_file_uploads' => 1,
					),

				)
			);
		return $meta_boxes;
	}
}
