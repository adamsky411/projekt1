<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<?php wp_head(); ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>
<body <?php body_class(); ?>>
